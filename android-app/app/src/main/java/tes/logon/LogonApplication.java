package tes.logon;

import android.app.Application;

import net.danlew.android.joda.JodaTimeAndroid;

import tes.logon.web.LoginService;

public class LogonApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
        LoginService.makeApplicationInstance(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        LoginService.destroyApplicationInstance();
    }
}
