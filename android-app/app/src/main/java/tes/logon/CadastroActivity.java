package tes.logon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class CadastroActivity extends AppCompatActivity {
    private EditText editTextEmail, getEditTextPassword, getGetEditTextConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
    }

    public void onClick(View v)
    {

        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        getEditTextPassword = (EditText) findViewById(R.id.editTextPassword);
        getGetEditTextConfirmPassword = (EditText) findViewById(R.id.editTextPassword2);

        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle("Confirmação");
        alert.setMessage("Usuário Salvo com sucesso!");
        alert.show();

        Intent intent = new Intent(CadastroActivity.this, HomeActivity.class);

        intent.putExtra("_EMAIL_", editTextEmail.getText().toString());

        startActivity(intent);
    }
}
