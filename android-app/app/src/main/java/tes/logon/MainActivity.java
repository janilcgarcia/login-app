package tes.logon;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import tes.logon.web.LoginService;

public class MainActivity extends AppCompatActivity {
    private EditText editTextEmail, getEditTextPassword;
    private Button btnLogon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        getEditTextPassword = (EditText) findViewById(R.id.editTextPassword);
        btnLogon = (Button) findViewById(R.id.btnLogon);
    }

    public void onClick(View v)
    {
        LoginService loginService = LoginService.getApplicationInstance();

        final MainActivity _this = this;

        setEnabled(false);

        loginService.login(editTextEmail.getText().toString(),
                getEditTextPassword.getText().toString(), new LoginService.CompletionHandler<Void>() {
                    @Override
                    public void onSuccess(Void value) {
                        Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                        intent.putExtra("_EMAIL_", editTextEmail.getText().toString());
                        startActivity(intent);
                    }

                    @Override
                    public void onError(Throwable ex) {
                        AlertDialog alert = new AlertDialog.Builder(_this).create();
                        alert.setTitle("Error");
                        alert.setMessage("Usuário ou Senha incorretos");
                        alert.show();
                        setEnabled(true);
                    }
                });
    }

    private void setEnabled(boolean enabled) {
        editTextEmail.setEnabled(enabled);
        getEditTextPassword.setEnabled(enabled);
        btnLogon.setEnabled(enabled);
    }
}
