package tes.logon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    private TextView textViewEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle("LogonNext");
        alert.setMessage("Seja bem vindo!");
        alert.show();

        Intent intent = getIntent();

        String email = intent.getStringExtra("_EMAIL_");

        textViewEmail = (TextView) findViewById(R.id.textViewEmail);

        textViewEmail.setText(email);
    }

    public void configurar(View v){
        Intent intent = new Intent(HomeActivity.this, ConfigureActivity.class);

        intent.putExtra("_EMAIL_", (String) intent.getStringExtra("_EMAIL_"));

        startActivity(intent);
    }

    public void sair(View v){
        Intent intent = new Intent(HomeActivity.this, MainActivity.class);

        startActivity(intent);
    }
}
