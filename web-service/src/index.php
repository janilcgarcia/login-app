<?php

namespace LoginApp;

define('__ROOT__', dirname(__FILE__));

require_once __ROOT__ . "/config/config.in.php";
require_once __ROOT__ . "/system/HTTPRequest.php";
require_once __ROOT__ . "/system/HTTPResponse.php";
require_once __ROOT__ . "/system/RequestDispatcher.php";
require_once __ROOT__ . "/system/Route.php";
require_once __ROOT__ . "/system/HTTPError.php";

require_once __ROOT__ . "/app/filters/DatabaseFilter.php";
require_once __ROOT__ . "/app/filters/AuthenticationFilter.php";

require_once __ROOT__ . "/app/models/UserDAO.php";
require_once __ROOT__ . "/app/views/UserResource.php";

function execute() {
  $user = new Views\UserResource();
  $databaseFilter = new Filters\DatabaseFilter(array(
    'userDao' => 'LoginApp\\Models\\UserDAO'
  ));

  $authenticationFilter = new Filters\AuthenticationFilter();
  $routes = array(
    new System\Route("/register", array($user, "register"), "POST"),
    new System\Route("/login", array($user, "login"), "POST"),
    new System\Route("/self", array($user, "info"), "GET"),
    new System\Route("/self/changepass", array($user, "changepass"), "PUT"),
    new System\Route("/self", array($user, "delete"), "DELETE")
  );

  $dispatcher = new System\RequestDispatcher($routes,
      array($databaseFilter, $authenticationFilter));
  $request = new System\HTTPRequest($_SERVER);

  $dispatcher->dispatch($request);
}

execute();
