<?php

namespace LoginApp\System;

require_once __ROOT__ . "/system/Route.php";
require_once __ROOT__ . "/system/HTTPError.php";
require_once __ROOT__ . "/system/HTTPResponse.php";

class RequestDispatcher {
  private $routes;
  private $filters;

  public function __construct($routes, $filters) {
    $this->routes = $routes;
    $this->filters = $filters;
  }

  public function dispatch($request) {
    try {
      $action = null;

      foreach ($this->filters as $filter) {
        $filter->handleRequest($request);
      }

      foreach ($this->routes as $route) {
        if ($action = $route->match($request)) {
          $resp = $action($request);
          break;
        }
      }

      if (!$action) {
        throw new HTTPError("Resource not found: " . $request->getMethod()
            . " " . $request->getPath(), 404);
      }
    } catch (HTTPError $herror) {
      $resp = $herror->makeJSONResponse();
    } catch (Exception $ex) {
      if (DEBUG) {
        $msg = $ex->getMessage();
      } else {
        $msg = "Ooops! Something went wrong!";
      }

      $resp = (new HTTPError($msg, 404))->makeJSONResponse();
    }

    $resp->render();
  }
}
