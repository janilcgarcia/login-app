<?php

namespace LoginApp\System;

require_once __ROOT__ . '/system/CIDictionary.php';

class HTTPRequest {
  private $method;
  private $path;
  private $headers;
  private $bodyBuffer;
  private $context;

  public function __construct($server_params) {
    $this->method = $server_params['REQUEST_METHOD'];
    $this->path = $server_params['PATH_INFO'];
    $this->headers = new CIDictionary();
    $this->context = array();
    $this->bodyBuffer = false;

    $regex = "/(?:REDIRECT_)?HTTP_(.*)/";
    foreach ($server_params as $key => $value) {
      $matches = array();
      if (preg_match($regex, $key, $matches)) {
        $this->headers->set($matches[1], $value);
      }
    }
  }

  public function getMethod() {
    return $this->method;
  }

  public function getPath() {
    return $this->path;
  }

  public function getHeader($name) {
    return $this->headers->get($name);
  }

  public function getBody() {
    if ($this->bodyBuffer === FALSE) {
      $this->bodyBuffer = null;

      if (strpos($this->getHeader('content-type'),
            'multipart/form-data') === FALSE) {
        $this->bodyBuffer = file_get_contents('php://input');
      }
    }

    return $this->bodyBuffer;
  }

  public function getJSON() {
    $body = $this->getBody();

    if ($body) {
      return json_decode($body);
    }

    return null;
  }

  public function setContext($name, $value) {
    return ($this->context[$name] = $value);
  }

  public function getContext($name) {
    return $this->context[$name];
  }
}
