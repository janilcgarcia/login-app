<?php

namespace LoginApp\System;

class CIDictionary {
  private $array;

  public function __construct() {
    $this->array = array();
  }

  public function set($name, $value) {
    $this->array[strtoupper($name)] = $value;
    return $this;
  }

  public function get($name) {
    return $this->array[strtoupper($name)];
  }

  public function exists($name) {
    return isset($this->array[strtoupper($name)]);
  }

  public function delete($name) {
    $value = $this->array[strtoupper($name)];
    unset($this->array[strtoupper($name)]);
    return $value;
  }

  public function getKeys() {
    return array_keys($this->array);
  }
}
