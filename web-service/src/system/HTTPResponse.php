<?php

namespace LoginApp\System;

require_once __ROOT__ . "/system/CIDictionary.php";

class HTTPResponse {
  private $status;
  private $headers;
  private $body;

  public function __construct() {
    $this->status = 200;
    $this->headers = new CIDictionary();
    $this->body = null;
  }

  public function make() {
    return new HttpResponse();
  }

  public function setBody($body) {
    $content_type = "text/plain";

    if (is_string($body)) {
      $this->body = $body;
    } else if (is_array($body)) {
      $this->body = json_encode($body);
      $content_type = 'application/json; charset=utf-8';
    } else {
      $this->body = (string) $body;
    }

    $this->headers->set('content-type', $content_type);
    return $this;
  }

  public function getHeader($name) {
    return $this->headers->get($name);
  }

  public function setHeader($name, $value) {
      $this->headers->set($name, $value);
      return $this;
  }

  public function removeHeader($name) {
    return $this->headers->delete($name);
  }

  public function getStatus() {
    return $this->status;
  }

  public function setStatus($code) {
    $this->status = $code;
    return $this;
  }

  public function render() {
    http_response_code((int) $this->status);

    foreach ($this->headers->getKeys() as $key) {
      header($this->prepareHeader($key, $this->headers->get($key)));
    }

    echo $this->body;
  }

  private function prepareHeader($key, $value) {
    $tokens = preg_split('/([_\-]+)/', $key, -1,
        PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

    foreach ($tokens as $ikey => $ivalue) {
      $tokens[$ikey] = ucwords(strtolower($ivalue));
    }

    $tokens[] = ": ";
    $tokens[] = $value;

    return implode('', $tokens);
  }
}
