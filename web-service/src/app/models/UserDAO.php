<?php

namespace LoginApp\Models;

class UserDAO
{
  private $pdo;

  public function __construct($pdo) {
    $this->pdo = $pdo;
  }

  private function _make_user($row) {
    return new User($row["name"], $row["pass_hash"],
        new \DateTimeImmutable($row["created_at"]),
        new \DateTimeImmutable($row["updated_at"]), $row["id"]);
  }

  public function get_by_id($id) {
    $stmt = $this->pdo->prepare("SELECT * FROM users WHERE id = ? LIMIT 1");
    $stmt->execute([$id]);

    $row = $stmt->fetch();

    if (!$row) {
      return null;
    }

    return $this->_make_user($row);
  }

  public function get_by_name($name) {
    $stmt = $this->pdo
        ->prepare("SELECT * FROM users WHERE name = ? LIMIT 1");
    $stmt->execute([$name]);

    $row = $stmt->fetch();
    if (!$row) {
      return null;
    }

    return $this->_make_user($row);
  }

  public function insert($user) {
    $stmt = $this->pdo->prepare(
      "INSERT INTO users(name, pass_hash) VALUES (:name, :pass_hash)");

    $this->pdo->beginTransaction();

    try {
      $stmt->execute(array(
        "name" => $user->get_name(),
        "pass_hash" => $user->get_pass_hash()
      ));
      $user->set_id($this->pdo->lastInsertId());
      $this->pdo->commit();
    } catch (PDOException $e) {
      $this->pdo->rollBack();
      throw $e;
    }
  }

  public function update($user) {
    $stmt = $this->pdo->prepare(
        "UPDATE users SET name = :name, pass_hash = :pass_hash WHERE id = :id");

    $this->pdo->beginTransaction();

    try {
      $stmt->execute(array(
        "name" => $user->get_name(),
        "pass_hash" => $user->get_pass_hash(),
        "id" => $user->get_id()
      ));

      $this->pdo->commit();
    } catch (PDOException $e) {
      $this->pdo->rollBack();
      throw $e;
    }
  }

  public function save($user) {
    if ($user->is_dirty()) {
      // It is a dirty, dirty user
      if (!$user->get_id()) $this->insert($user);
      else $this->update($user);
    }
  }

  public function delete($user) {
    $stmt = $this->pdo->prepare("DELETE FROM users WHERE id = ?");

    $this->pdo->beginTransaction();
    try {
      $stmt->execute([$user->get_id()]);
      $this->pdo->commit();
    } catch (PDOException $e) {
      $this->pdo->rollBack();
      throw $e;
    }
  }
}
