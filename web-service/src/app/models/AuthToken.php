<?php

namespace LoginApp\Models;

require_once __ROOT__ . "/util.php";

class AuthToken {
  private $userID;
  private $expiration;

  public function __construct($userID, $exp) {
    $this->userID = $userID;
    $this->expiration = $exp;
  }

  public function buildToken() {
    $expireAt = $this->expiration->format('U');

    $token = explode("=", base64_encode(json_encode(array(
        "user" => $this->userID,
        "expireAt" => $expireAt
    ))))[0];

    $sig = explode("=", base64_encode(\sodium_crypto_auth(
        $token, APP_KEY)))[0];

    return $token . "." . $sig;
  }

  public static function make($user, $now = null, $validFor = null) {
    if (!$now) {
      $now = new \DateTimeImmutable('now', new \DateTimeZone('UTC'));
    }

    if (!$validFor) {
      $validFor = new \DateInterval("PT5H");
    }

    return new AuthToken($user->get_id(), $now->add($validFor));
  }

  public static function verify($token) {
    $now = new \DateTimeImmutable('now', new \DateTimeZone('UTC'));
    $parts = explode('.', $token);
    $token = $parts[0];
    $sig = base64_decode(self::fixMissingPadding($parts[1]));

    if (!\sodium_crypto_auth_verify($sig, $token, APP_KEY)) {
      return null;
    }

    $token = json_decode(base64_decode(self::fixMissingPadding($token)));

    $expireAt = \DateTimeImmutable
        ::createFromFormat('U', (int) $token->expireAt);

    if ($now > $expireAt) {
      return null;
    }

    return $token;
  }

  private static function fixMissingPadding($b64) {
      $len = strlen($b64);
      return $b64 . str_repeat("=", abs(\LoginApp\Util\mod($len, -4)));
  }
}
