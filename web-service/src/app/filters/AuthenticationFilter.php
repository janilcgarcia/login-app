<?php

namespace LoginApp\Filters;

require_once __ROOT__ . "/system/interfaces.php";
require_once __ROOT__ . "/app/models/AuthToken.php";

use LoginApp\System\Filter;
use LoginApp\Models\AuthToken;
use LoginApp\System\HTTPError;

class AuthenticationFilter implements Filter {
  public function handleRequest($req) {
    $auth = $req->getHeader("Authorization");

    if (!$auth) {
      $req->setContext('user', null);
      return;
    }

    $parts = preg_split('/\s+/', $auth);
    if ($parts[0] != 'Token') {
      throw new HTTPError(
        array(
          "Invalid Authorization header",
          "Invalid authorization scheme: " . $parts[0]
        ),
        401
      );
    }

    $token = AuthToken::verify($parts[1]);

    if (!$token) {
      throw new HTTPError(array("Invalid or expired token"), 401);
    }

    $user = $req->getContext("userDao")->get_by_id($token->user);

    if (!$user) {
      throw new HTTPError(array("Authentication user is no longer valid"), 403);
    }

    $req->setContext("user", $user);
  }
}
